from collections import deque


class Cache:
    def __init__(self, max_len: int) -> None:
        self.storage = dict()
        self.queue = deque()
        self.max_len = max_len

    def __setitem__(self, key, value) -> None:
        if len(self.queue) == self.max_len:  # if full
            key_to_remove = self.queue.pop()  # dump from queue
            self.storage.pop(key_to_remove)  # dump from storage
        self.storage[key] = value
        self.queue.appendleft(key)

    def __getitem__(self, key):
        value = self.storage[key]
        del self.queue[self.queue.index(key)]
        self.queue.appendleft(key)
        return value

    def __contains__(self, key) -> bool:
        return key in self.storage
