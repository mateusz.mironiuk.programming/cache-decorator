from collections import deque
from inspect import Signature
from typing import Optional, Iterable, Any


def validate_arguments(
    arguments: Optional[Iterable[str]], signature: Signature
) -> None:
    if not arguments:
        return
    function_arguments = signature.parameters.keys()
    for argument in arguments:
        if argument not in function_arguments:
            raise AttributeError(f"unknown argument {argument}")
