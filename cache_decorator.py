from functools import wraps, partial
from inspect import signature as get_signature
from typing import Optional, Callable, Iterable

from cache import Cache
from validators import validate_arguments


def cached(
    function: Callable = None,
    /,
    *,
    max_len=128,
    arguments: Optional[Iterable[str]] = None,
) -> Callable:

    if function is None:
        return partial(cached, max_len=max_len, arguments=arguments)

    signature = get_signature(function)
    validate_arguments(arguments=arguments, signature=signature)
    cache = Cache(max_len=max_len)

    @wraps(function)
    def wrapper(*args, **kwargs):
        key = tuple(
            value
            for key, value in signature.bind(*args, **kwargs).arguments.items()
            if (key in arguments if arguments else True)
        )
        if key not in cache:
            cache[key] = function(*args, **kwargs)
        return cache[key]

    return wrapper
