from collections import deque

from cache import Cache


DEFAULT_CACHE_SIZE = 10


def test_setitem():
    cache = Cache(max_len=DEFAULT_CACHE_SIZE)
    cache["key_1"] = "value_1"
    cache["key_2"] = "value_2"

    assert cache.storage == {"key_1": "value_1", "key_2": "value_2"}
    assert cache.queue == deque(("key_2", "key_1"))


def test_setitem_with_max_len_exceeded():
    cache = Cache(max_len=2)
    cache["key_1"] = "value_1"
    cache["key_2"] = "value_2"
    cache["key_3"] = "value_3"

    assert cache.storage == {"key_2": "value_2", "key_3": "value_3"}
    assert cache.queue == deque(("key_3", "key_2"))


def test_getitem_moves_key_to_start():
    cache = Cache(max_len=DEFAULT_CACHE_SIZE)
    cache["key_1"] = "value_1"
    cache["key_2"] = "value_2"
    cache["key_3"] = "value_3"

    assert cache.storage == {"key_3": "value_3", "key_2": "value_2", "key_1": "value_1"}
    assert cache.queue == deque(("key_3", "key_2", "key_1"))

    _ = cache["key_2"]

    assert cache.storage == {"key_3": "value_3", "key_2": "value_2", "key_1": "value_1"}
    assert cache.queue == deque(("key_2", "key_3", "key_1"))
