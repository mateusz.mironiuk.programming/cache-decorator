import pytest

from cache_decorator import cached


def test_caching():
    generator = (value for value in ("cached_value", "non-cached value"))

    @cached
    def func():
        nonlocal generator
        return next(generator)

    assert func() == "cached_value"
    assert func() == "cached_value"


def test_valid_arguments():
    @cached(arguments=["a"])
    def f(a, b):
        return a + b

    assert f("hello", "World") == "helloWorld"
    assert f("hello", "Europe") == "helloWorld"


def test_invalid_arguments():
    with pytest.raises(AttributeError):

        @cached(arguments=["c"])
        def f(a, b):
            return a + b
